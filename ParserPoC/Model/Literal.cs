﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC.Model
{
    public class Literal : Expression
    {
        public string? StringValue { get; }
        public double? NumberValue { get; }

        public Literal(string value)
        {
            StringValue = value;
        }

        public Literal(double value)
        {
            NumberValue = value;
        }
    }
}
