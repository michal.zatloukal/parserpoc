﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC.Model
{
    public class Property : Expression
    {
        public List<string> Path { get; }
        public Property(List<string> path)
        {
            Path = path;
        }
    }
}
