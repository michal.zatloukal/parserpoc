﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC.Model
{
    public class ExpressionGroup : Expression
    {
        public Expression InnerExpression { get; }
        public ExpressionGroup(Expression innerExpression)
        {
            InnerExpression = innerExpression;
        }
    }
}
