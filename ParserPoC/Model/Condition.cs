﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC.Model
{
    public class Condition : Expression
    {
        public Property Property { get; }
        public EqualsOperator EqualsOperator { get; }
        public Literal Literal { get; }
        public Condition(Property property, EqualsOperator equalsOperator, Literal literal)
        {
            Property = property;
            EqualsOperator = equalsOperator;
            Literal = literal;
        }
    }
}
