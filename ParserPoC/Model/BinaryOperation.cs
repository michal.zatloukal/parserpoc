﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC.Model
{
    public class BinaryOperation : Expression
    {
        public Expression Left { get; }
        public LogicalOperator Operator { get; }
        public Expression Right { get; }
        public BinaryOperation(Expression left, LogicalOperator @operator, Expression right)
        {
            Left = left;
            Operator = @operator;
            Right = right;
        }
    }
}
