﻿using ParserPoC.Model;
using sly.parser.generator;
using System;

namespace ParserPoC
{
    class Program
    {
        public static void Main(string[] args)
        {
            var parserInstance = new QueryParser();
            var builder = new ParserBuilder<ExpressionToken, Expression>();
            var build = builder.BuildParser(parserInstance, ParserType.EBNF_LL_RECURSIVE_DESCENT, $"{typeof(QueryParser).Name}_expressions");
            var parser = build.Result;

            var parseResult = parser.Parse("(properties.reported.test = 12.3 AND tags.deviceType = 'astronaut') OR (tags.deviceType = -8.44 AND tags.deviceType = '1234b')");
        }
    }
}
