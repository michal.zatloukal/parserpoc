﻿using ParserPoC.Model;
using sly.lexer;
using sly.parser.generator;
using sly.parser.parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC
{
    public class QueryParser
    {
        [Production("expression: LPAREN QueryParser_expressions RPAREN")]
        public Expression ExpressionGroup(Token<ExpressionToken> lparen, Expression expression, Token<ExpressionToken> rparen)
        {
            return new ExpressionGroup(expression);
        }

        // expression: expression OR expression
        [Operation("OR", Affix.InFix, Associativity.Left, 1)]
        public Expression ExpressionOr(Expression leftExpression, Token<ExpressionToken> andToken, Expression rightExpression)
        {
            return new BinaryOperation(leftExpression, LogicalOperator.OR, rightExpression);
        }

        // expression: expression AND expression
        [Operation("AND", Affix.InFix, Associativity.Left, 2)]
        public Expression ExpressionAnd(Expression leftExpression, Token<ExpressionToken> andToken, Expression rightExpression)
        {
            return new BinaryOperation(leftExpression, LogicalOperator.AND, rightExpression);
        }

        [Operand]
        [Production("expression: condition")]
        public Expression ExpressionCondition(Expression condition)
        {
            return condition;
        }

        [Production("condition: property EQUALS literal")]
        public Expression Condition(Property property, Token<ExpressionToken> equalsToken, Literal literal)
        {
            EqualsOperator equalsOperator = equalsToken.TokenID switch
            {
                ExpressionToken.EQUALS => EqualsOperator.Equals,
                ExpressionToken.NOT_EQUALS => EqualsOperator.NotEquals,
                _ => EqualsOperator.Equals
            };

            return new Condition(property, equalsOperator, literal);
        }

        [Production("literal: INT")]
        public Expression LiteralInt(Token<ExpressionToken> intToken)
        {
            return new Literal(intToken.IntValue);
        }

        [Production("literal: MINUS INT")]
        public Expression LiteralIntMinus(Token<ExpressionToken> minusToken, Token<ExpressionToken> intToken)
        {
            return new Literal(-intToken.IntValue);
        }

        [Production("literal: DOUBLE")]
        public Expression LiteralDouble(Token<ExpressionToken> doubleToken)
        {
            return new Literal(doubleToken.DoubleValue);
        }

        [Production("literal: MINUS DOUBLE")]
        public Expression LiteralDoubleMinus(Token<ExpressionToken> minusToken, Token<ExpressionToken> doubleToken)
        {
            return new Literal(-doubleToken.DoubleValue);
        }

        [Production("literal: STRING_VALUE")]
        public Expression LiteralString(Token<ExpressionToken> stringValueToken)
        {
            return new Literal(stringValueToken.StringWithoutQuotes.Replace("'", ""));
        }

        [Production("property : IDENTIFIER (DOT [d] IDENTIFIER)*")]
        public Expression Property(Token<ExpressionToken> head, List<Group<ExpressionToken, Expression>> tail)
        {
            List<string> path = new List<string>
            {
                head.StringWithoutQuotes
            };

            foreach(var group in tail)
            {
                string? tailPath = group.Items.FirstOrDefault()?.Token.StringWithoutQuotes;

                if (tailPath is not null)
                {
                    path.Add(tailPath);
                }
            }

            return new Property(path);
        }
    }
}
