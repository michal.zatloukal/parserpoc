﻿using sly.lexer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserPoC
{
    public enum ExpressionToken
    {
        [Lexeme("AND")] AND = 1,
        [Lexeme("OR")] OR = 2,
        [Lexeme("=")] EQUALS = 3,
        [Lexeme("!=")] NOT_EQUALS = 4,

        // float number 
        [Lexeme("[0-9]+\\.[0-9]+")] DOUBLE = 5,
        // integer        
        [Lexeme("[0-9]+")] INT = 6,
        [Lexeme("[a-zA-Z][a-zA-Z0-9]+")] IDENTIFIER = 7,
        [Lexeme("'[a-zA-Z0-9]+'")] STRING_VALUE = 8,

        [Lexeme("'")] APOSTROPHE = 9,
        [Lexeme("\\.")] DOT = 10,
        [Lexeme("\\(")] LPAREN = 11,
        [Lexeme("\\)")] RPAREN = 12,
        [Lexeme("-")] MINUS = 13, 

        // a whitespace
        [Lexeme("[ \\t]+", true)] WS = 9998,

        [Lexeme("[\\n\\r]+", true, true)] EOL = 9999,
    }
}
